
App.factory('Tax', function($http, Url) {

    var factory = {};

    factory.loadListData = function() {
        return $http({
            method: 'GET',
            url: Url.get("tax/backoffice_list/load"),
            cache: true,
            responseType:'json'
        });
    };

    factory.findAll = function() {

        return $http({
            method: 'GET',
            url: Url.get("tax/backoffice_list/findall"),
            cache: true,
            responseType:'json'
        });
    };

    factory.save = function(tax) {

        return $http({
            method: 'PUT',
            data: tax,
            url: Url.get("tax/backoffice_list/save"),
            responseType:'json'
        });

    };

    factory.delete = function(tax) {

        return $http({
            method: 'PUT',
            data: tax,
            url: Url.get("tax/backoffice_list/delete"),
            responseType:'json'
        });

    };

    return factory;
});
