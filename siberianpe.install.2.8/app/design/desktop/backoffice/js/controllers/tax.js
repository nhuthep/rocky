App.config(function($routeProvider) {

    $routeProvider.when(BASE_URL+"/tax/backoffice_list", {
        controller: 'TaxListController',
        templateUrl: BASE_URL+"/tax/backoffice_list/template"
    });

}).controller("TaxListController", function($scope, Header, Label, Tax, SectionButton) {

    $scope.header = new Header();
    $scope.header.button.left.is_visible = false;
    $scope.content_loader_is_visible = true;

    $scope.button = new SectionButton(function() {
        $scope.add();
    });

    Tax.loadListData().success(function(data) {
        $scope.header.title = data.title;
        $scope.header.icon = data.icon;
    });

    Tax.findAll().success(function(data) {
        $scope.taxes = data.taxes;
        $scope.countries = data.countries;
        $scope.prepareCountries();
    }).finally(function() {
        $scope.content_loader_is_visible = false;
    });

    $scope.add = function() {
        $scope.taxes.push({
            id: null,
            rate: "",
            country_code: null,
            orig: {
                rate: "",
                country_code: null
            },
            is_changed: false,
            loader_is_visible: false
        });
        $scope.prepareCountries();
    };

    $scope.change = function(tax) {
        tax.is_changed = tax.orig.rate != tax.rate || tax.orig.country_code != tax.country_code;
    };

    $scope.save = function(tax) {

        if(!tax.country_code || !tax.rate || !tax.is_changed) {
            return;
        }

        tax.loader_is_visible = true;

        Tax.save(tax).success(function(data) {
            if(angular.isObject(data)) {
                $scope.message.setText(data.message)
                    .isError(false)
                    .show()
                ;

                if(!tax.id || tax.country_code != tax.orig.country_code) {
                    $scope.prepareCountries();
                }

                tax.id = data.tax_id;
                tax.orig.country_code = tax.country_code;
                tax.orig.rate = tax.rate;
                tax.is_changed = false;
            } else {
                $scope.message.setText(Label.save.error)
                    .isError(true)
                    .show()
                ;
            }
        }).error(function(data) {

            var message = Label.save.error;
            if(angular.isObject(data)) {
                message = data.message;
            }
            $scope.message.setText(message)
                .isError(true)
                .show()
            ;

        }).finally(function() {
            tax.loader_is_visible = false;
        });
    };

    $scope.delete = function(tax) {

        var index = $scope.taxes.indexOf(tax);

        if(angular.isDefined(index)) {

            if(tax.id) {

                tax.loader_is_visible = true;

                Tax.delete(tax).success(function (data) {
                    if(angular.isObject(data) && data.success) {
                        $scope.taxes.splice(index, 1);
                        $scope.prepareCountries();
                    }
                }).error(function (data) {

                    var message = Label.loading.error;
                    if(angular.isObject(data)) {
                        message = data.message;
                    }
                    $scope.message.setText(message)
                        .isError(true)
                        .show()
                    ;

                }).finally(function () {
                    tax.loader_is_visible = false;
                });
            } else {
                $scope.taxes.splice(index, 1);
            }
        }
    };

    $scope.prepareCountries = function() {
        $scope.filtered_countries = {};
        var countries_taken = new Array();

        for(var i = 0; i < $scope.taxes.length; i++) {
            countries_taken.push($scope.taxes[i].country_code);
        }

        for(var i in $scope.countries) {
            if(countries_taken.indexOf(i) == -1) {
                $scope.filtered_countries[i] = $scope.countries[i];
            }
        }

        for(var i = 0; i < $scope.taxes.length; i++) {
            console.log($scope.filtered_countries);
            var countries = {};
            angular.extend(countries, $scope.filtered_countries)
            console.log(countries);

            var code = $scope.taxes[i].country_code;
            countries[code] = $scope.countries[code];
            $scope.taxes[i].countries = countries;
        }

    }

});
