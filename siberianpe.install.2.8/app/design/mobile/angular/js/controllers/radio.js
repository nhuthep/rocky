App.config(function ($routeProvider) {

    $routeProvider.when(BASE_URL + "/radio/mobile_radio/index/value_id/:value_id", {
        controller: 'RadioController',
        template: ''
    });

}).controller('RadioController', function ($scope, $routeParams, $location, Url, Radio, MediaMusicTracksLoaderService, MediaMusicPlayerService) {

    Radio.value_id = $routeParams.value_id;

    $scope.loadContent = function () {
        Radio.find().success(function (data) {

            MediaMusicPlayerService.init(document);

            var tracksLoader = MediaMusicTracksLoaderService.loadSingleTrack({
                name: data.radio.title,
                streamUrl: data.radio.url
            });

            MediaMusicPlayerService.playTracks(tracksLoader, 0, true, true);
            
        });
    }

    $scope.loadContent();

});