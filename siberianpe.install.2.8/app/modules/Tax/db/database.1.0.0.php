<?php

$this->query("

    CREATE TABLE `tax` (
      `tax_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `country_code` VARCHAR(10) NOT NULL,
      `rate` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      PRIMARY KEY (`tax_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

");
