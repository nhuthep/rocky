<?php

class Tax_Backoffice_ListController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Taxes"),
            "icon" => "fa-file",
        );

        $this->_sendHtml($html);

    }

    public function findallAction() {

        $tax = new Tax_Model_Tax();
        $taxes = $tax->findAll();
        $data = array("taxes" => array(), "countries" => array());
        $countries = Zend_Registry::get('Zend_Locale')->getTranslationList('Territory', null, 2);

        foreach($taxes as $tax) {
            $data["taxes"][] = array(
                "id" => $tax->getId(),
                "country_code" => $tax->getCountryCode(),
                "country_name" => $countries[$tax->getCountryCode()],
                "rate" => $tax->getRate(),
                "orig" => array(
                    "country_code" => $tax->getCountryCode(),
                    "rate" => $tax->getRate()
                )
            );
        }

        $data["countries"] = $countries;

        $this->_sendHtml($data);

    }

    public function saveAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                if(empty($data["country_code"])) {
                    throw new Exception($this->_("Please select a country"));
                }

                if(isset($data["rate"])) {
                    $data["rate"] = str_replace(array(",", "%"), array(".", ""), $data["rate"]);
                    if(!is_numeric($data["rate"])) {
                        throw new Exception($this->_("Please, enter a valid tax rate."));
                    }
                } else {
                    throw new Exception($this->_("Please, enter a tax rate."));
                }

                $tax = new Tax_Model_Tax();
                if(!empty($data["id"])) {
                    $tax->find($data["id"]);
                }

                $tax->addData($data)->save();

                $data = array(
                    "success" => 1,
                    "message" => $this->_("Info successfully saved"),
                    "tax_id" => $tax->getId()
                );

            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);
        }

    }

    public function deleteAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                if(empty($data["id"])) {
                    throw new Exception($this->_("An error occurred while loading. Please, try again later."));
                }

                $tax = new Tax_Model_Tax();
                $tax->find($data["id"]);
                $tax->delete();

                $data = array(
                    "success" => 1,
                    "message" => $this->_("Info successfully saved"),
                    "tax_id" => $data["id"]
                );

            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);
        }

    }

}
