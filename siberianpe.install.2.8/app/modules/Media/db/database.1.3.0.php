<?php

$this->query("
    CREATE TABLE `media_gallery_music` (
        `gallery_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `value_id` int(11) unsigned NOT NULL,
        `artwork_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `position` int(11) NOT NULL,
        PRIMARY KEY (`gallery_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `media_gallery_music_album` (
        `album_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `gallery_id` int(11) unsigned NOT NULL,
        `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `artwork_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `podcast_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `artist_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        PRIMARY KEY (`album_id`),
        KEY `KEY_GALLERY_ID` (`gallery_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `media_gallery_music_elements` (
        `position_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `gallery_id` int(11) unsigned DEFAULT NULL,
        `album_id` int(11) unsigned DEFAULT NULL,
        `track_id` int(11) unsigned DEFAULT NULL,
        `position` int(11) NOT NULL,
        PRIMARY KEY (`position_id`),
        KEY `KEY_GALLERY_ID` (`gallery_id`),
        KEY `KEY_ALBUM_ID` (`album_id`),
        KEY `KEY_TRACK_ID` (`track_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    CREATE TABLE `media_gallery_music_track` (
        `track_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `album_id` int(11) unsigned DEFAULT NULL,
        `gallery_id` int(11) unsigned DEFAULT NULL,
        `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `artwork_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `artist_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `album_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `purchase_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `stream_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `position` int(11) NOT NULL,
        PRIMARY KEY (`track_id`),
        KEY `KEY_ALBUM_ID` (`album_id`),
        KEY `KEY_GALLERY_ID` (`gallery_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    ALTER TABLE `media_gallery_music_album`
        ADD FOREIGN KEY `FK_GALLERY_ID`(`gallery_id`) REFERENCES `media_gallery_music` (`gallery_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `media_gallery_music_elements`
        ADD FOREIGN KEY `FK_GALLERY_ID` (`gallery_id`) REFERENCES `media_gallery_music` (`gallery_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_ALBUM_ID` (`album_id`) REFERENCES `media_gallery_music_album` (`album_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_TRACK_ID` (`track_id`) REFERENCES `media_gallery_music_track` (`track_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$this->query("
    ALTER TABLE `media_gallery_music_track`
        ADD FOREIGN KEY `FK_ALBUM_ID` (`album_id`) REFERENCES `media_gallery_music_album` (`album_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        ADD FOREIGN KEY `FK_GALLERY_ID` (`gallery_id`) REFERENCES `media_gallery_music` (`gallery_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");


// Musics Gallery
$library = new Media_Model_Library();
$library->setName('Musics')->save();

$icon_paths = array(
    '/musics/music1.png',
    '/musics/music2.png',
    '/musics/music3.png',
    '/musics/music4.png',
    '/musics/music5.png',
    '/musics/music6.png',
    '/musics/music7.png',
    '/musics/music8.png',
    '/musics/music9.png'
);

$icon_id = 0;
foreach($icon_paths as $key => $icon_path) {
    $datas = array('library_id' => $library->getId(), 'link' => $icon_path, 'can_be_colorized' => 1);
    $image = new Media_Model_Library_Image();
    $image->setData($datas)->save();

    if($key == 0) $icon_id = $image->getId();
}

$datas = array(
    'library_id' => $library->getId(),
    'icon_id' => $icon_id,
    'code' => 'music_gallery',
    'name' => 'Audio',
    'model' => 'Media_Model_Gallery_Music',
    'desktop_uri' => 'media/application_gallery_music/',
    'mobile_uri' => 'media/mobile_gallery_music_playlists/',
    'only_once' => 0,
    'is_ajax' => 1,
    'position' => 110
);
$option = new Application_Model_Option();
$option->setData($datas)->save();

