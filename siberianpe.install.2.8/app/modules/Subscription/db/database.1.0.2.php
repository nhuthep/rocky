<?php

$this->query("
    ALTER TABLE `subscription_application`
      ADD `profile_id` VARCHAR(30) NULL DEFAULT NULL AFTER `app_id`,
      ADD `correlation_id` VARCHAR(30) NULL DEFAULT NULL AFTER `profile_id`;
");
