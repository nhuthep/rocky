<?php

class Subscription_Model_Subscription extends Core_Model_Default {

    const MONTHLY_FRENQUENCY = "Monthly";
    const YEARLY_FRENQUENCY = "Yearly";

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Subscription_Model_Db_Table_Subscription';
        return $this;
    }

    public function isActive() {
        return $this->getData("is_active");
    }

    public function toOrder() {

        $order = new Sales_Model_Order();
        $admin = $this->getAdmin();
        $tax = new Tax_Model_Tax();
        $tax_rate = null;
        $countries = Siberian_Locale::getEuroZoneTerritoryDatas();
        $store_country = System_Model_Config::getValueFor("company_country");
        $admin_country = $admin->getCountryCode();

        // If the store is European, the client is European but doesn't have a VAT Number, he pays the store tax
        if(array_key_exists($store_country, $countries) AND array_key_exists($admin_country, $countries)) {
            if(!$admin->getVatNumber()) {
                $tax->find($store_country, "country_code");
            }
        } else {
            $tax->find($admin_country, "country_code");
        }

        $tax_rate = $tax->getId() ? $tax->getRate() : 0;

        $order->setTaxRate($tax_rate);

        if($this->getSetupFee()) {
            $qty = 1;
            $line = new Sales_Model_Order_Line();
            $line->setRef($this->getRef())
                ->setName($this->_("Setup Fee"))
                ->setPriceExclTax($this->getSetupFee())
                ->setQty($qty)
                ->setTotalPriceExclTax($this->getSetupFee() * $qty)
            ;

            $order->addLine($line);
        }

        if($this->getRegularPayment()) {
            $qty = 1;
            $name = $this->_("%s Subscription for %s", $this->getPaymentFrequency(), $this->getCurrentApplication()->getName());
            $line = new Sales_Model_Order_Line();
            $line->setRef($this->getRef())
                ->setName($name)
                ->setPriceExclTax($this->getRegularPayment())
                ->setQty($qty)
                ->setTotalPriceExclTax($this->getRegularPayment() * $qty)
                ->setIsRecurrent(1)
            ;

            $order->addLine($line);
        }

        $order->calcTotals();

        return $order;

    }

    public function getPriceHtml() {

        $html = array();

        if ($this->getSetupFee()) {
            $html[] = $this->getFormattedSetupFee();
        }

        if ($this->getRegularPayment()) {

            if($this->getSetupFee()) {
                $html[] = "+";
            }

            $html[] = $this->getFormattedRegularPayment();

            if($this->getPaymentFrequency() == self::MONTHLY_FRENQUENCY) {
                $html[] = "/ Month";
            } else if($this->getPaymentFrequency() == self::YEARLY_FRENQUENCY) {
                $html[] = "/ Year";
            }
        }

        return implode(" ", $html);

    }

    public function getSetupFee() {
        $setup_fee = $this->getData("setup_fee");
        return $setup_fee > 0 ? $setup_fee : null;
    }

    public function getRegularPayment() {
        $regular_payment = $this->getData("regular_payment");
        return $regular_payment > 0 ? $regular_payment : null;
    }

}
