<?php

class Subscription_Backoffice_Application_EditController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Subscription"),
            "icon" => "fa-list-ul",
        );

        $this->_sendHtml($html);

    }

    public function findAction() {


        $subscription = new Subscription_Model_Subscription_Application();
        $subscription->find($this->getRequest()->getParam("subscription_app_id"));

        $data = array();
        if($subscription->getSubscriptionAppId()) {
            $data["subscription"] = $subscription->getData();

            $date = new Zend_Date($data["subscription"]['expire_at'], Zend_Date::ISO_8601);
            $data["subscription"]['expire_at'] = $date->toString('MM/dd/yyyy');

            $data["section_title"] = $this->_("Edit the subscription %s", $subscription->getSubscriptionAppId() );
        } else {
            $data["section_title"] = $this->_("Create a new subscription");
        }

        $countries = Zend_Registry::get('Zend_Locale')->getTranslationList('Territory', null, 2);
        asort($countries, SORT_LOCALE_STRING);
        $data["country_codes"] = $countries;

        $this->_sendHtml($data);

    }

    public function saveAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                $subscription = new Subscription_Model_Subscription_Application();
                $dummy = new Admin_Model_Admin();
                $dummy->find($data["email"], "email");
                $isNew = true;

                if(!empty($data["subscription_app_id"])) {
                    $subscription->find($data["subscription_app_id"]);
                    $isNew = !$subscription->getSubscriptionAppId();
                }

                $arrayDate = explode('/',$data["expire_at"]);
                $data["expire_at"] = $arrayDate[2].'-'.$arrayDate[0].'-'.$arrayDate[1];

                $paypal_response = true;
                if($data["is_active"] != $subscription->getIsActive()) {
                    if(!$data["is_active"]) {
                        $paypal_recurring_action = "SUSPEND";
                    }else{
                        $paypal_recurring_action = "REACTIVATE";
                    }

                    $paypal = new Payment_Model_Paypal();
                    $paypal_response = $paypal->manageRecurring($data["profile_id"], $paypal_recurring_action);
                }

                if($paypal_response) {
                    $subscription->addData($data);
                    $subscription->save();
                }else{
                    throw new Exception($this->_("An error occurred while saving. Please try again later."));
                }

                $data = array(
                    "success" => 1,
                    "message" => $this->_("User successfully saved")
                );

            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);
        }

    }

}
