<?php

class Subscription_Backoffice_EditController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $title = "Create a new plan";
        if($this->getRequest()->getParam("id")) {
            $title = "Edit the plan";
        }

        $html = array(
            "title" => $title,
            "icon" => "fa-credit-card",
        );

        $this->_sendHtml($html);

    }

    public function findAction() {

        $data = array();
        $subscription = new Subscription_Model_Subscription();
        $subscription->find($this->getRequest()->getParam("id"));
        if($subscription->getId() AND $subscription->isActive()) {
            $data["plan"] = $subscription->getData();
        }

        $this->_sendHtml($data);

    }

    public function saveAction() {

        if($data = Zend_Json::decode($this->getRequest()->getRawBody())) {

            try {

                if(empty($data["payment_frequency"])) {
                    $data["regular_payment"] = null;
                }
                if(!empty($data["regular_payment"])) {
                    $data["regular_payment"] = str_replace(",", ".", $data["regular_payment"]);
                }
                if(!empty($data["setup_fee"])) {
                    $data["setup_fee"] = str_replace(",", ".", $data["setup_fee"]);
                }

                if(empty($data["name"])) {
                    throw new Exception($this->_("Please, enter a name."));
                }
                if(empty($data["description"])) {
                    throw new Exception($this->_("Please, enter a description."));
                }
                if(empty($data["setup_fee"]) AND empty($data["regular_payment"])) {
                    throw new Exception($this->_("Your subscription must have at least one price"));
                }

                if(!empty($data["setup_fee"]) AND !Zend_Validate::is(floatval($data["setup_fee"]), "float")) {
                    throw new Exception($this->_("The setup fee is not a valid price (e.g. 12.34"));
                }
                if(!empty($data["regular_payment"]) AND !Zend_Validate::is(floatval($data["regular_payment"]), "float")) {

                    if($data["payment_frequency"] == "Monthly") {
                        throw new Exception($this->_("The monthly price is not a valid price (e.g. 12.34)"));
                    } else {
                        throw new Exception($this->_("The yearly price is not a valid price (e.g. 12.34)"));
                    }

                }

                $subscription = new Subscription_Model_Subscription();
                if(!empty($data["subscription_id"])) {
                    $subscription->find($data["subscription_id"]);
                }

                if(empty($data["setup_fee"])) {
                    $data["setup_fee"] = null;
                }

                if(empty($data["regular_payment"])) {
                    $data["regular_payment"] = null;
                }

                $subscription->setData($data)
                    ->save()
                ;

                $data = array(
                    "success" => 1,
                    "message" => $this->_("Subscription successfully saved")
                );

            } catch(Exception $e) {
                $data = array(
                    "error" => 1,
                    "message" => $e->getMessage()
                );
            }

            $this->_sendHtml($data);
        }

    }

}
