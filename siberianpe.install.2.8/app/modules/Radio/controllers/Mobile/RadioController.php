<?php

class Radio_Mobile_RadioController extends Application_Controller_Mobile_Default {

    public function _toJson($radio){
        $json = array(
            "url" => $url = addslashes($radio->getLink()),
            'title' => $radio->getTitle()
        );

        return $json;
    }

    public function findAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {
            
            try {
                
                $radio_repository = new Radio_Model_Radio();
                $radio = $radio_repository->find(array('value_id' => $value_id));
                
                $data = array("radio" => $this->_toJson($radio));
            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

        } else {
            $data = array('error' => 1, 'message' => $this->_("An error occurred while loading. Please try again later."));
        }

        $this->_sendHtml($data);

    }

}