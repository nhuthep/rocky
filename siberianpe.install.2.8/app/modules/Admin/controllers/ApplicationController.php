<?php

class Admin_ApplicationController extends Admin_Controller_Default
{
    public function indexAction() {
        $this->_forward('list');
        return $this;
    }

    public function listAction() {
        $this->loadPartials();
    }

    public function createpostAction() {

        if($datas = $this->getRequest()->getPost()) {

            try {
                if(empty($datas['name'])) {
                    throw new Exception($this->_('Please, enter an application name'));
                }

                $application = new Application_Model_Application();
                $application->setName($datas['name'])
                    ->setAdminId($this->getSession()->getAdminId())
                    ->addAdmin($this->getSession()->getAdmin())
                    ->save()
                ;

                $this->getSession()->showTemplates = true;

                $html = array(
                    'url' => $this->getUrl('admin/application/set', array('id' => $application->getId(), 'redirect_to' => 'edit'))
                );

            }
            catch(Exception $e) {
                $html = array(
                    'message' => $e->getMessage(),
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->_sendHtml($html);

        }

        return $this;

    }

    public function deleteAction() {

        if($id = $this->getRequest()->getParam('id')) {
            try {
                $application = new Application_Model_Application();
                $application->find($id)->setIsActive(0)->save();
                $html = array('success' => '1');
            }
            catch(Exception $e) {
                $html = array(
                    'message' => "Une erreur s'est produite lors de la suppression de votre application",
                    'message_button' => 1,
                    'message_loader' => 1
                );
            }

            $this->_sendHtml($html);

        }
    }

    public function setAction() {

        if($id = $this->getRequest()->getParam('id')) {

            $application = new Application_Model_Application();
            $application->find($id);
            $admin_ids = $application->getTable()->getAdminIds($application->getId());
            if(!$application->getId() OR !in_array($this->getSession()->getAdmin()->getId(), $admin_ids)) {
                $this->_redirect('admin/application/list');
            }
            else {
                // Passe l'id de l'appli en session
                $this->getSession()->setAppId($id);
                $this->_redirect('application/customization');
            }

            return $this;
        }

    }

}
