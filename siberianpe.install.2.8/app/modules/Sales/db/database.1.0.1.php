<?php

$this->query("
    ALTER TABLE `sales_order_line`
      ADD `is_recurrent` TINYINT(1) NOT NULL DEFAULT 0 AFTER `total_price_excl_tax`
    ;

    ALTER TABLE `sales_invoice_line`
      ADD `is_recurrent` TINYINT(1) NOT NULL DEFAULT 0 AFTER `total_price_excl_tax`
    ;
");
