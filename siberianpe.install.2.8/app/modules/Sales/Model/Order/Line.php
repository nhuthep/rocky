<?php

class Sales_Model_Order_Line extends Core_Model_Default {

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Sales_Model_Db_Table_Order_Line';
        return $this;
    }

    public function toInvoice() {

        $line = new Sales_Model_Invoice_Line();
        $line->setData($this->getData())
            ->setOrderLineId($this->getId())
            ->unsId()
            ->setId(null)
        ;

        return $line;
    }

    public function getQty() {
        $qty = 0;
        if($this->getData('qty')) {
            $qty = round($this->getData('qty'));
        }

        return $qty;
    }
}

?>
