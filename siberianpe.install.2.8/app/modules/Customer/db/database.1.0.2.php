<?php

$this->query("
    ALTER TABLE `comment`
        ADD FOREIGN KEY `FK_CUSTOMER_ID` (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");