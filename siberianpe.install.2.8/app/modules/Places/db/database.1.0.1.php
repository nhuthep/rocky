<?php

$this->query("
    ALTER TABLE `cms_application_page_block_address`
        ADD `latitude` VARCHAR(20) NULL DEFAULT NULL AFTER `address`,
        ADD `longitude` VARCHAR(20) NULL DEFAULT NULL AFTER `latitude`
    ;
");

$this->query("ALTER TABLE `cms_application_page` ADD picture VARCHAR(255) NULL DEFAULT NULL AFTER `content`;");
