<?php

class Mcommerce_Mobile_Sales_ConfirmationController extends Mcommerce_Controller_Mobile_Default {

    public function cancelAction() {
        if(!$this->getRequest()->getParam("value_id")) {
            $this->_redirect("/mcommerce/mobile_sales_confirmation/cancel/",  array("value_id" => $this->getCurrentOptionValue()->getId()));
            return $this;
        } else {
            $this->forward('cancel', 'index', 'Front', $this->getRequest()->getParams());
        }
    }

    // multi.siberian.xtraball.local/544f5c785b4e1/mcommerce/mobile_sales_confirmation/confirm?token=EC-439587114G6373212&PayerID=YAKJYF8KA8U2J
    public function confirmAction() {
        if (!$this->getRequest()->getParam("value_id")) {
            $this->_redirect("/mcommerce/mobile_sales_confirmation/confirm/",array_merge($this->getRequest()->getParams(),array("value_id" => $this->getCurrentOptionValue()->getId())));
            return $this;
        } else {
            $this->forward('confirm', 'index', 'Front', $this->getRequest()->getParams());
        }
    }
}