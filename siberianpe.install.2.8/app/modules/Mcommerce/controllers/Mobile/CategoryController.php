<?php

class Mcommerce_Mobile_CategoryController extends Mcommerce_Controller_Mobile_Default {
  
    public function findallAction() {

        if($value_id = $this->getRequest()->getParam('value_id')) {

            try {

                $category_id = $this->getRequest()->getParam('category_id');
                $current_category = new Folder_Model_Category();

                if($category_id) {
                    $current_category->find($category_id, 'category_id');
                }

                $object = $this->getCurrentOptionValue()->getObject();

                if(!$object->getId() OR ($current_category->getId() AND $current_category->getRootCategoryId() != $object->getRootCategoryId())) {
                    throw new Exception($this->_('An error occurred during process. Please try again later.'));
                }

                if(!$current_category->getId()) {
                    $current_category = $object->getRootCategory();
                }

                $data = array("categories" => array());

                $subcategories = $current_category->getChildren();

                foreach($subcategories as $subcategory) {
                    $data["categories"][] = array(
                       "title" => $subcategory->getTitle(),
                        "subtitle" => $subcategory->getSubtitle(),
                        "picture" => $subcategory->getPictureUrl(),
                        "url" => $this->getPath("mcommerce/mobile_category", array("value_id" => $value_id, "category_id" => $subcategory->getId()))
                    );
                }

                $products = $current_category->getProducts();
                $color = $this->getApplication()->getBlock('background')->getImageColor();
                
                $current_store = $this->getStore();

                foreach($products as $product) {
                    
                    $taxRate = $current_store->getTax($product->getTaxId())->getRate();
                    
                    $data["categories"][] = array(
                        "title" => $product->getName(),
                        "subtitle" => $product->getPrice() > 0 ? $product->formatPrice($product->getPrice() * (1 + $taxRate / 100)) : strip_tags($product->getDescription()),
                        "picture" => $product->getPictureUrl(),
                        "url" => $product->getPath("mcommerce/mobile_product", array("value_id" => $value_id, "product_id" => $product->getId()))
                    );
                }
                
                $data["cover"] = array(
                    "title" => $current_category->getTitle(),
                    "subtitle" => $current_category->getSubtitle(),
                    "picture" => $current_category->getPictureUrl()
                );

                $data["page_title"] = $current_category->getTitle();

            }
            catch(Exception $e) {
                $data = array('error' => 1, 'message' => $e->getMessage());
            }

            $this->_sendHtml($data);

        }

    }

}