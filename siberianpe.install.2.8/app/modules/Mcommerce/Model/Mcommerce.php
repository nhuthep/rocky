<?php

class Mcommerce_Model_Mcommerce extends Core_Model_Default {

    protected $_default_store;
    protected $_stores;
    protected $_carts;
    protected $_orders;
    protected $_taxes;
    protected $_catalog;
    protected $_root_category;
    protected $_products;

    public function __construct($params = array()) {
        parent::__construct($params);
        $this->_db_table = 'Mcommerce_Model_Db_Table_Mcommerce';
        return $this;
    }

    public function delete() {

        $products = $this->getProducts();
        $catalog = $this->getCatalog();

        foreach($this->getOrders() as $order) {
            $order->delete();
        }

        foreach($this->getCarts() as $cart) {
            $cart->delete();
        }

        foreach($this->getTaxes() as $tax) {
            $tax->delete();
        }

        foreach($this->getStores() as $store) {
            $store->delete();
        }

        parent::delete();

        foreach($products as $product) {
            $product->delete();
        }

        $catalog->delete();

        return $this;

    }

    public function prepareFeature($option_value) {
        
        parent::prepareFeature($option_value);
        if(!$this->getId()) {
            $catalog_option = new Application_Model_Option();
            $catalog_option->find('catalog', 'code');
            $catalog_option_value = new Application_Model_Option_Value();
            // Ajoute les données
            $catalog_option_value->addData(array(
                'option_id' => $catalog_option->getId(),
                'app_id' => $option_value->getAppId(),
                'position' => 0,
                'is_active' => 1,
                'is_visible' => 0
            ))->save();

            $root_category = new Folder_Model_Category();
            $root_category->setTitle($this->_('Category'))
                ->setPos(1)
                ->save()
            ;

            $this->setValueId($option_value->getId())
                ->setCatalogValueId($catalog_option_value->getId())
                ->setRootCategoryId($root_category->getId())
                ->save()
            ;

        }

        return $this;
    }

    public function getStores() {

        if(!$this->_stores) {
            $store = new Mcommerce_Model_Store();
            $this->_stores = $store->findAll(array('mcommerce_id' => $this->getId(), 'is_visible' => 1));
        }

        return $this->_stores;

    }

    public function getCarts() {

        if(!$this->_carts) {
            $cart = new Mcommerce_Model_Cart();
            $this->_carts = $cart->findAll(array('mcommerce_id' => $this->getId()), 'cart_id DESC');
        }

        return $this->_carts;

    }

    public function getOrders() {

        if(!$this->_orders) {
            $order = new Mcommerce_Model_Order();
            $this->_orders = $order->findAll(array('mcommerce_id' => $this->getId()), 'order_id DESC');
        }

        return $this->_orders;

    }

    public function getDefaultStore() {

        if(!$this->_default_store) {
            $this->_default_store = $this->getStores()->rewind()->current();
            if(!$this->_default_store) $this->_default_store = new Mcommerce_Model_Store();
        }

        return $this->_default_store;

    }

    public function getRootCategory() {

        if(!$this->_root_category) {
            $this->_root_category = new Folder_Model_Category();
            $this->_root_category->find($this->getRootCategoryId());
            $this->_root_category->setIsRootCategory(1);
        }

        return $this->_root_category;
    }

    public function getTaxes() {

        if(!$this->_taxes) {
            $tax = new Mcommerce_Model_Tax();
            $this->_taxes = $tax->findAll(array('mcommerce_id' => $this->getId()));
        }

        return $this->_taxes;

    }

    public function getCatalog() {

        if(!$this->_catalog) {
            $this->_catalog = new Application_Model_Option_Value();
            $this->_catalog->find($this->getCatalogValueId());
//            $this->_catalog = $catalog_value->getObject();
        }

        return $this->_catalog;

    }

    public function getProducts() {

        if(!$this->_products) {
            $product = new Catalog_Model_Product();
            $this->_products = $product->findAll(array('mcommerce_id' => $this->getId()));
        }

        return $this->_products;

    }

}
