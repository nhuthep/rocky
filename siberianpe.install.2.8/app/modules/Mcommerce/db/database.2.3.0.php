<?php

// Create the app_id field
$this->query("ALTER TABLE `catalog_product_group` ADD `app_id` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `group_id`");


// Fetch all the existing groups
$group = new Catalog_Model_Product_Group();
$groups = array();

foreach ($group->findAll() as $group) {
    $groups[$group->getId()] = $group;
}

// Fetch all the group_id / app_id
$select = $this->_db->select()
    ->from(array("cpgv" => "catalog_product_group_value"), array("group_id", "value_id"))
    ->join(array("cpg" => "catalog_product_group"), "cpg.group_id = cpgv.group_id", array("title", "app_id"))
    ->join(array("cp" => "catalog_product"), "cp.product_id = cpgv.product_id", array())
    ->join(array("aov" => "application_option_value"), "aov.value_id = cp.value_id", array("app_id"))
;

$collection = $this->_db->fetchAll($select);

foreach ($collection as $data) {

    if (!empty($groups[$data["group_id"]])) {
        $group = $groups[$data["group_id"]];
        // Set the app_id to the group
        if (!$group->getAppId()) {
            $group->setAppId($data["app_id"])
                ->save()
            ;
        }
    }
}

// Remove all the unused group & options
foreach ($groups as $group) {
    if (!$group->getAppId()) {
        foreach ($group->getOptions() as $option) {
            $option->delete();
        }
        $group->delete();
    }
}

foreach ($groups as $group) {

    foreach ($collection as $data) {

        if ($data["group_id"] == $group->getId() AND $data["app_id"] != $group->getAppId()) {
            $this->_db->delete("catalog_product_group_option_value", array("group_value_id = ?" => $data["value_id"]));
            $this->_db->delete("catalog_product_group_value", array("value_id = ?" => $data["value_id"]));
        }

    }

}
