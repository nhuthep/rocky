<?php
/**
 * Siberian_Controller_Request_Http
 *
 * HTTP request object for use with Zend_Controller family.
 * Add set & getMediaUrl
 *
 * @uses Zend_Controller_Request_Abstract
 * @package Siberian_Controller
 * @subpackage Request
 */
class Siberian_Controller_Request_Http extends Zend_Controller_Request_Http
{

    protected $_is_backoffice = false;
    protected $_backoffice_url;
    protected $_language_code;
    protected $_force_language_code = false;
    protected $_is_native;
    protected $_application;
    protected $_application_key = false;
    protected $_is_installing = false;
    protected $_mediaUrl;

    public function setPathInfo($pathInfo = null) {

        parent::setPathInfo($pathInfo);

        $path = $this->_pathInfo;
        $paths = explode('/', trim($path, '/'));
        $language = !empty($paths[0]) ? $paths[0] : '';

        if(in_array($language, Core_Model_Language::getLanguageCodes())) {
            $this->_language_code = $language;
            unset($paths[array_search($language, $paths)]);
            $paths = array_values($paths);
        }

        if(!$this->isInstalling()) {

            $application = new Application_Model_Application();
            $application->find($this->getHttpHost(), "domain");
            if($application->getId()) {
                $this->_application = $application;
                $this->_application_key = false;
            }
            else if(!empty($paths[0])) {

//                if($paths[0] == $this->_backoffice_url) {
//                    $this->_is_backoffice = true;
//                } else {

                    $application->find($paths[0], "key");
                    if($application->getId()) {
                        $this->_application = $application;
                        $this->_application_key = $application->getKey();
                        unset($paths[0]);
                    }
//                }

            }
        }

        $paths = array_diff($paths, Core_Model_Language::getLanguageCodes());
        $paths = array_values($paths);
        $this->_pathInfo = '/'.implode('/', $paths);

        $detector = new Mobile_Detect();
        $this->_is_native = $detector->isNative();

        return $this;
    }

//    public function setBackofficeUrl($url) {
//        $this->_backoffice_url = $url;
//        return $this;
//    }
//
//    public function isBackoffice() {
//        return $this->_is_backoffice;
//    }

    public function setMediaUrl($url) {
        $this->_mediaUrl = $url;
        return $this;
    }

    public function getMediaUrl() {
        $url = $this->_mediaUrl;
        if(!$url) {
            $url = $this->_baseUrl;
        }

        return $url;
    }

    public function getLanguageCode() {
        return $this->_language_code;
    }

    public function setLanguageCode($language_code) {
        $this->_language_code = $language_code;
        return $this;
    }

    public function addLanguageCode($language_code = null) {
        if(!is_null($language_code)) {
            $this->_force_language_code = true;
            $this->_language_code = $language_code;
            return $this;
        } else {
            return $this->_force_language_code;
        }
    }

    public function isApplication() {
        return !is_null($this->_application);
    }

    public function isNative() {
        return $this->_is_native;
    }

    public function getApplication() {
        return $this->_application;
    }

    public function useApplicationKey() {
        return (bool) $this->_application_key;
    }

    public function getApplicationKey() {
        return $this->_application_key;
    }

    public function setApplicationKey($app_key) {
        $this->_application_key = $app_key;
    }

    public function isInstalling($isInstalling = null) {
        if(!is_null($isInstalling)) {
            $this->_is_installing = $isInstalling;
            return $this;
        } else {
            return $this->_is_installing;
        }
    }

}
