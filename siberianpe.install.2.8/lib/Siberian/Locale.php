<?php
/**
 * Base class for localization
 *
 * @category  Zend
 * @package   Zend_Locale
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 */
class Siberian_Locale extends Zend_Locale
{

    /**
     * Class wide Euro Zone Locale Constants
     *
     * @var array $_euroZoneTerritoryDatas
     */
    private static $_euroZoneTerritoryDatas = array(
        'AT' => 'de_AT',
        'BE' => 'nl_BE',
        'BG' => 'bg_BG',
        'CY' => 'el_CY',
        'CZ' => 'cs_CZ',
        'DE' => 'de_DE',
        'DK' => 'da_DK',
        'EE' => 'et_EE',
        'ES' => 'es_ES',
        'FI' => 'fi_FI',
        'FR' => 'fr_FR',
        'GB' => 'en_GB',
        'GR' => 'el_GR',
        'HR' => 'hr_HR',
        'HU' => 'hu_HU',
        'IE' => 'en_IE',
        'IT' => 'it_IT',
        'LT' => 'lt_LT',
        'LU' => 'fr_LU',
        'LV' => 'lv_LV',
        'MT' => 'mt_MT',
        'NL' => 'nl_NL',
        'PL' => 'pl_PL',
        'PT' => 'pt_PT',
        'RO' => 'ro_RO',
        'SE' => 'sv_SE',
        'SI' => 'sl_SI',
        'SK' => 'sk_SK'
    );

    public static function getEuroZoneTerritoryDatas() {
        return self::$_euroZoneTerritoryDatas;
    }

}
