//
//  ViewController.h
//  Siberian Angular
//
//  Created by Adrien Sala on 08/07/2014.
//  Copyright (c) 2014 Adrien Sala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
#import "RNCachingURLProtocol.h"
#import "webViewController.h"
#import "Contact.h"
#import "common.h"

@interface ViewController : UIViewController <UIWebViewDelegate, CLLocationManagerDelegate, ContactDelegate> {
    BOOL webViewIsLoaded;
    NSURL *webviewUrl;
    CLLocationManager *locationManager;
}

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet loaderView *loader;

@property (strong, nonatomic) UIImageView *splashScreen;
@property (strong, nonatomic) UIImage *splashScreenImage;


@end
